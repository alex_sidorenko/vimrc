# My personal bash aliases that are shared across all workstations I use

alias gb='git branch'
alias gm='git merge'
alias gba='git branch -a'
alias gc='git commit -v'
alias gd='git difftool -d --no-symlink'
alias gdc='git difftool -d --no-symlink --cached'
alias gl='git pull'
alias gp='git push'
alias gst='git status .'
alias ga='git add'
alias gco='git checkout'
alias gg='git log --graph --pretty=oneline --abbrev-commit'
alias gr='grep --color -n -R --include *.cpp --include *.h --include *.c . -e '

cf()
{
    (test -z $1 || test -z $2) && echo -e "Create a tarball with a nice progress bar\nUsage: cf TARBALL_NAME FILES" ||
    (
    echo -e "Calculating files total size..."
    _TotalSize=`du --total --summarize  --bytes ${*:2} | tail -1 | cut --fields 1`
    echo -e "Total size is $_TotalSize bytes"
    echo -e "Creating tarball..."
    tar cf - ${*:2} | pv --progress --rate --bytes --size $_TotalSize > $1
    )
}

xf()
{
    (test -z $1) && echo -e "Extract a tarball with a nice progress bar\nUsage: xf TARBALL_NAME [PATH]" ||
    (
    echo -e "Extracting tarball..."
    if [ -z "$2" ]
    then
        pv $1 | tar xf - 
    else
        pv $1 | tar xf - -C $2
    fi
    )
}
