colorscheme leo256
set vb t_vb=

set guifont=Droid\ Sans\ Mono\ Slashed\ 14

if has("win32")
    set guifont=Droid\ Sans\ Mono\ Slashed:h11
endif

" Mac OS-specific
if has("gui_macvim")
    set guifont=Droid\ Sans\ Mono\ Slashed:h18

    " Unmap Command-F
    macmenu Edit.Find.Find\.\.\. key=<nop>
    "macmenu MacVim.Hide\ MacVim key=<nop>
    "set fullscreen
endif
