"""""""""""""""""""OPTIONS""""""""""""""""""""""
" Colorscheme for console. It will be replaced with leo256 in gvimrc.
colorscheme pablo

" Force filetype auto-detection
filetype plugin on

" Highlight search results
set hlsearch

" Remove menu and other useless stuff
set go-=m go-=T go-=l go-=L go-=r go-=R go-=b go-=F 

" Put a nice $ to indicate area being edit
set cpoptions+=$

" Enable virtual edit mode
set virtualedit=all

" Automatically break lines longer than 80
"set textwidth=80

" Highlight characters that are wider than 80
"augroup vimrc_autocmds
  "autocmd BufEnter * highlight OverLength ctermbg=red ctermfg=white guibg=#591919
  "autocmd BufEnter * match OverLength /\%81v.\+/
"augroup END

" Wildmenu for a nice autocompleation
set wildmenu

" When performing substitute, replace all the mathes on the line
set gdefault 

" Show line numbers
set number

" A nice status bar. Taken from Derek Wyatt
set stl=%f\ %m\ Line:%l/%L[%p%%]\ Col:%v\ Buf:#%n\ [%b][0x%B][%{g:BuildType}]

" tell VIM to always put a status line in, even if there is only one window
set laststatus=2

" Allows us to hide unsaved buffers
set hidden

" By default, ignore case when searching
set ignorecase

" Black magic. A method to toggle QuickFix window
command -bang -nargs=? QFix call QFixToggle(<bang>0)
function! QFixToggle(forced)
  if exists("g:qfix_win") && a:forced == 0
    cclose
    unlet g:qfix_win
  else
    copen 15
    let g:qfix_win = bufnr("$")
  endif
endfunction
" Map QuickFix toggle to F2
map <F2> :QFix<CR>

" Set tabs
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set autoindent
set smartindent

" Highlight tabs
set lcs=tab:>-,trail:#
set list

" Enable incremental search
set incsearch

" Force enable syntax highlighting
syntax on

" Enable spell checking
set spell
set spelllang=en_us

" REQUIRES PLUGIN: ProtoDef
" Tell ProtoDef where pullproto.pl is
let g:protodefprotogetter = '~/.vim/pullproto.pl'
" A nicier mapping
nmap <silent> <A-g> <leader>PN

" Disable backup and swap files
set nobackup
set noswapfile

" Disable annoying sound bell
set visualbell
set noerrorbells
set t_vb=

" Force backspace to behave in correct way
set backspace=indent,eol,start

"""""""""""""""""""KEY MAPPINGS"""""""""""""""""""""

" Alt + J to type :tjump 
nmap <A-j> :tjump 

" Swap * and g* commands
nnoremap * g*
vnoremap * g*

nnoremap g* *
vnoremap g* *

" Swap <C+]> and g<C+j> commands
nnoremap <c-]> g<c-]>
vnoremap <c-]> g<c-]>

nnoremap g<c-]> <c-]>
vnoremap g<c-]> <c-]>

" Ctrl + S to save all the buffers
nmap <C-s> :wall<CR>

" F4 to toggle tabs highlighting
map <F4> :set list!<CR>

" Backspace to switch to previous buffer
map <Backspace> :b#<CR>

" REQUIRES PLUGIN: NERDCommenter
" Sexy comment lines
nmap <Space>j <Leader>cs
vmap <Space>j <Leader>cs
" Adds comment to the end of the line
nmap <Space>h <Leader>cA
vmap <Space>h <Leader>cA
" Toggle comment for selected lines
nmap <Space>k <Leader>c<Space>
vmap <Space>k <Leader>c<Space>

" Windows-specific
if has("win32")
    " Start Vim maximized.
    au GUIEnter * simalt ~x "x on an English Windows version. n on a French one

    " Open file for edit in SD
    nmap <A-e> :silent !sd edit <C-r>%<CR>:edit<CR>

    " Revert file in SD
    nmap <A-r> :silent !sd revert <C-r>%<CR>:edit<CR>
endif


" A lightweight wrapper for grep/findstr (Unix/Linux and Windows respectively)
if has("win32")
    " Windows - findstr wrapper
    nmap <C-f> :exe 'cexpr system("findstr -N -R -S -C:\"' . input("Regexp to grep [case-sensetive]: ", expand('<cword>')) . '\" ' . input("File extensions: ", "*.cpp *.h *.c") . '")'<CR>
    nmap <A-f> :exe 'cexpr system("findstr -N -R -S -I -C:\"' . input("Regexp to grep [case-insensetive]: ", expand('<cword>')) . '\" ' . input("File extensions: ", "*.cpp *.h *.c") . '")'<CR>
else
    " Unix/Linux - grep wrapper
    nmap <C-f> :exe 'cexpr system("grep -I -n -R --include *.cpp --include *.h --include *.c -e \"' . input("Regexp to grep [case-sensitive]: ", expand('<cword>')) . '\" .")'<CR>
    nmap ƒ :exe 'cexpr system("grep -I -i -n -R --include *.cc *.cpp --include *.h --include *.c -e \"' . input("Regexp to grep [case-insensetive]: ", expand('<cword>')) . '\" .")'<CR>
endif

" Mac OS X Detection
"if has("unix")
  let s:uname = system("uname")
  let g:MacOS = 0
  if s:uname == "Darwin\n"
    let g:MacOS = 1
  endif
"endif

map <C-j> :colder<CR>
map <C-k> :cnewer<CR>

" Default to release build
let g:BuildType="release"

function SetNinjaAsBuildSystem()
    " Make ninja default build system
    execute 'set makeprg=ninja\ -C\ build/' . g:BuildType
endfunction

call SetNinjaAsBuildSystem()

function SetBuildType()
    let g:BuildType=tolower(input("Build type: ", g:BuildType))
    call SetNinjaAsBuildSystem()
endfunction

map <F3> :call SetBuildType()<CR>

if g:MacOS == 1
    " Key bindings to travel QuickFix window contents
    map <D-j> :cp<CR>
    map <D-k> :cn<CR>
    map <D-l> :cc<CR>

    " REQUIRES PLUGIN: A
    " Switch between header/source
    noremap <D-d> :A<CR>

    " REQUIRES PLUGIN: FuzzyFinder
    " Open FuzzyFinder for buffers
    map ˙ :FufBuffer<CR>

    " REQUIRES PLUGIN: FuzzyFinder
    " Open FuzzyFinder for tags
    map <D-n> :FufTag<CR>

    " Call make
    map <D-b> <Esc>:wall<CR>:make<CR>

    set vb
else
    " Key bindings to travel QuickFix window contents
    map <A-j> :cp<CR>
    map <A-k> :cn<CR>
    map <A-l> :cc<CR>

    " REQUIRES PLUGIN: A
    " Switch between header/source
    noremap <A-d> :A<CR>

    " REQUIRES PLUGIN: FuzzyFinder
    " Open FuzzyFinder for buffers
    map <A-h> :FufBuffer<CR>

    " REQUIRES PLUGIN: FuzzyFinder
    " Open FuzzyFinder for tags
    map <A-n> :FufTag<CR>

    if has("win32")
        " Hack for MS-only
        map <C-b> :cfile <C-r>%<CR>
    else
        " Call make
        map <C-b> :make<CR>
    endif

    " Shortcuts for copy to clipboard
    nmap <A-c> "+y
    vmap <A-c> "+y

    " Shortcut for pasting from clipboard
    vmap <A-v> "+p
    nmap <A-v> "+p
    imap <A-v> <C-r>+
    cmap <A-v> <C-r>+
endif

